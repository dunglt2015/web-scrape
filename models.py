from peewee import *
import config
db = MySQLDatabase(config.DATABASE['db'], 
					host = config.DATABASE['host'], 
					port = config.DATABASE['port'], 
					user = config.DATABASE['user'], 
					passwd = config.DATABASE['passwd'],
					use_unicode = config.DATABASE['use_unicode'],
					charset = config.DATABASE['charset'])
db.execute_sql("SET NAMES utf8 COLLATE utf8_unicode_ci;")

class Job(Model):
	job_id = CharField()
	job_title = CharField()
	area1 = CharField()
	employment_value = CharField()
	job_category = CharField()
	salary_range = CharField()
	posted_date = DateTimeField()
	age = TextField()
	requirements = TextField()
	salary = TextField()
	period = TextField()
	working_hour = TextField()
	location = TextField()
	description = TextField()
	company_name = CharField()
	company_website = CharField()
	representative = CharField()
	email = CharField()
	tel = CharField()
	fax = CharField()
	pr = TextField()

	class Meta:
		database = db