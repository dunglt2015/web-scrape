from lxml import html
import utility_function as util
import requests
import peewee
from models import Job

	
def scrape_data_a_page(page_url):
	page = requests.get(page_url)
	tree_html_page = html.fromstring(page.content)
	#job_id and job_title
	job_id_string = tree_html_page.xpath('//h4[@class="title"]/span/text()')[0]
	job_id = util.get_id_number(job_id_string)
	job_title = tree_html_page.xpath('//h4[@class="title"]/text()')[0]
	#area1, employment_value, job_category and salary_range
	basic_info = tree_html_page.xpath('//span[@class="post_category"]/a/text()')
	area1 = basic_info[0]
	employment_value = basic_info[1]
	job_category = basic_info[2]
	salary_range = basic_info[3]
	#posted_date
	date_string = tree_html_page.xpath('//span[@class="date_from"]/text()')[0]
	posted_date = util.get_posted_date(date_string)
	#age, requirements, salary, period, working_hour and location
	td = tree_html_page.xpath('//td')
	age = td[0].text_content()
	requirements = td[1].text_content()
	salary = td[2].text_content()
	period = td[3].text_content()
	working_hour = td[4].text_content()
	location = td[5].text_content()
	#description
	list_description = tree_html_page.xpath('//div[@itemprop="description"]//text()')
	description = "".join(list_description)
	#company_name and company_website
	company_name_text = tree_html_page.xpath('//a[@itemprop="Organization"]/text()')
	if company_name_text:
		company_name = company_name_text[0]
		company_website = tree_html_page.xpath('//a[@itemprop="Organization"]/@href')[0]	
	else:
		company_name = tree_html_page.xpath('//li[@itemprop="Organization"]/text()')[0]
		company_website = ""
	#representative and email
	representative = tree_html_page.xpath('//a[@onclick="return mailto_confirm()"]/text()')[0]
	email = tree_html_page.xpath('//a[@onclick="return mailto_confirm()"]/@href')[0]
	#tel
	tel_string = tree_html_page.xpath('//li[@type="circle"][3]/text()')[0]
	tel = util.get_tel(tel_string)
	#fax and pr
	more_info_1 = tree_html_page.xpath('//li[@type="circle"][4]/text()')
	if more_info_1:
		temp_1 = more_info_1[0]
		if temp_1[:3] == "Fax":
			fax = temp_1[4:]
			more_info_2 = tree_html_page.xpath('//li[@type="circle"][5]/text()')
			if more_info_2:
				temp_2 = "".join(more_info_2)
				pr = temp_2[3:]
		else:
			fax = ""
			pr = temp_1[3:]

	job = Job(job_id = job_id,
		job_title = job_title,
		area1 = area1,
		employment_value = employment_value,
		job_category = job_category,
		salary_range = salary_range,
		posted_date = posted_date,
		age = age,
		requirements = requirements,
		salary = salary,
		period = period,
		working_hour = working_hour,
		location = location,
		description = description,
		company_name = company_name,
		company_website = company_website,
		representative = representative,
		email = email,
		tel = tel,
		fax = fax,
		pr = pr)
	job.save()

def get_page_count(main_url):
	main_page = requests.get(main_url)
	tree_html_main_page = html.fromstring(main_page.content)
	pages_navigation = tree_html_main_page.xpath('//span[@class="pagenavi-box-a"]/a/@href')
	last_page = pages_navigation[-1]
	page_count = util.get_max_page(last_page) 
	#3442 pages, set smaller to test :)
	#page_count = 3
	return page_count

def scrape_data():
	print "Start scrape data!"
	try:
		Job.create_table()
	except Exception as e:
		print "Job table already exist!"
	exist_list_job_id = []
	exist_id_count = 0
	for job in Job.select(Job.job_id):
		exist_list_job_id.append(job.job_id)
		exist_id_count += 1
	print "______________________BEGIN_SCRAPE________________________"
	print "%d jobs has found" % exist_id_count

	main_url = 'https://4510m.in/'
	page_count = get_page_count(main_url)
	#Get list job id
	list_id_number = []
	for i in range(1, page_count + 1):
		list_page_url = main_url + "/page/" + str(i)
		list_page = requests.get(list_page_url)
		tree_html_list_page = html.fromstring(list_page.content)

		list_id_string_per_page = tree_html_list_page.xpath('//h4[@class="title"]/span/text()')
		list_id_number_per_page = util.get_list_id_number(list_id_string_per_page)
		list_id_number.extend(list_id_number_per_page)
	#print 'List ID: ', list_id_number 

	update_id_count = 0
	for i in range(0, len(list_id_number)):
		if list_id_number[i] not in exist_list_job_id:
			#print("New Job Offer!")
			update_id_count += 1
			page_url = main_url + str(list_id_number[i]) + "/"
			scrape_data_a_page(page_url)

	print "%d jobs has updated" % update_id_count
	job_count = update_id_count + exist_id_count
	print "4510m.in posted %d jobs" % job_count
	print "_______________________END_SCRAPE__________________________\n"

