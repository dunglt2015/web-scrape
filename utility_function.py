from datetime import datetime

def get_list_id_number(list_id_string):
	list_id_number = []

	for id in list_id_string:
		id_number = id[3:]
		list_id_number.append(id_number)
	
	return list_id_number

def get_id_number(id_string):
	id_number = id_string[3:]
	
	return id_number

def get_posted_date(date_string):
	date_string = date_string[5:]
	posted_date = datetime.strptime(date_string, "%Y/%m/%d %H:%M")

	return posted_date

def get_max_page(last_page_string):
	page_url = "http://4510m.in/page/"
	last_page = last_page_string.split(page_url, 1)[1]
	last_page_number = int(last_page.split("/", 1)[0])

	return last_page_number

def get_tel(tel_string):
	tel = tel_string[4:]

	return tel
