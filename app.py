from flask import Flask
from scrape_data import *
import requests
import threading
import time
    
def start_loop():
        not_started = True
        while not_started:
            print('In start loop')
            try:
                r = requests.get('http://127.0.0.1:5000/scrape')
                if r.status_code == 200:
                    print('Server started, start scrape_loop')
                    not_started = False
                    not_scraped = True
                    while not_scraped:
                    	scrape_data()
                    	time.sleep(604800)
                    	
                print(r.status_code)
            except:
                print('Server not yet started')
            time.sleep(2)

def start_runner():
    print('Server started?')
    thread = threading.Thread(target=start_loop)
    thread.start()

app = Flask(__name__)
app.config.from_object(__name__)


@app.route('/scrape')
def start_server():
	return "Hello" 

if __name__ == '__main__':
	start_runner()
	app.run(debug=True, use_reloader=False)

